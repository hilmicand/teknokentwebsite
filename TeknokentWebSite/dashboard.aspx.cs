﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data;
using MySql.Data.MySqlClient;


namespace TeknokentWebSite
{
    public partial class dashboard : System.Web.UI.Page
    {
        private string user_id = "";
        private bool connected = false;
        private string name = "", sname = "", mail = "", phone = "";

        private string[] roles = new string[] { null };
        private string[] all_of_roles = new string[] { null };

        private ArrayList ids = new ArrayList();
        private ArrayList d0 = new ArrayList();
        private ArrayList d1 = new ArrayList();
        private ArrayList d2 = new ArrayList();
        private ArrayList d3 = new ArrayList();
        private ArrayList d4 = new ArrayList();
        private ArrayList d5 = new ArrayList();
        private ArrayList d6 = new ArrayList();
        private ArrayList d7 = new ArrayList();
        private ArrayList d8 = new ArrayList();
        private ArrayList d9 = new ArrayList();

        private MySqlConnection mysql;

        Button btn;

        protected void Page_Load(object sender, EventArgs e)
        {
            checkSessions();
            opt1.ServerClick += Opt1_ServerClick;
            opt2.ServerClick += Opt2_ServerClick;
            opt111.ServerClick += Opt111_ServerClick;
            button_addon2.ServerClick += Button_ServerClick;
            
        }

        private void Btn_Click(object sender, EventArgs e)
        {
            user.InnerHtml = "Düzenle....";
        }

        private void Button_ServerClick(object sender, EventArgs e)
        {
            string id_ = srctext.Value;
            search(id_);
        }

        private void Opt111_ServerClick(object sender, EventArgs e)
        {
            Session["user_id"] = user_id;
            Response.Redirect("add_item.aspx");
        }

        private void Opt1_ServerClick(object sender, EventArgs e)
        {
            Session["user_id"] = user_id;
            Response.Redirect("add_item.aspx");
        }

        private void Opt2_ServerClick(object sender, EventArgs e)
        {
            Session["user_id"] = user_id;
            Response.Redirect("remove_item.aspx");
        }

        private void checkSessions()
        {
            if (Session["user_id"] != null)
            {
                user_id = Session["user_id"].ToString();
                opendb();

                if(connected)
                {
                    getUserInfo(user_id);
                    getAccess(user_id);
                    showTable();
                }
            }
            else
            {
                Response.Redirect("login.aspx");
            }
        }

        private void opendb()
        {
            mysql = new MySqlConnection("Database=teknokent_portal;server=127.0.0.1;uid=sqladmin;Password=sqladmin789_");
            mysql.Open();

            if(mysql.State == System.Data.ConnectionState.Open)
            {
                connected = true;
            }
            else
            {
                connected = false;
            }
        }

        private void getUserInfo(string id)
        {
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM users WHERE user_id='" + id + "'", mysql);
            MySqlDataReader reader = cmd.ExecuteReader();

            if(reader.Read())
            {
                name = reader["name"].ToString();
                sname = reader["sname"].ToString();
                mail = reader["email"].ToString();
                phone = reader["phone"].ToString();

                user.InnerHtml = "Hoşgeldiniz " + name + " " + sname;
            }
            else
            {
                content.InnerHtml = "<script>alert('Kişi bilgileri yuklenirken hata olustu...');</script>";
                Response.Redirect("login.aspx");
            }

            reader.Close();
        }

        private void getAccess(string id)
        {
            if(mysql.State == System.Data.ConnectionState.Open)
            {
                MySqlCommand cmd = new MySqlCommand("SELECT role_id FROM user_roles WHERE user_id='" + id + "'", mysql);
                MySqlDataReader reader = cmd.ExecuteReader();

                string r = "";
                while (reader.Read())
                {
                    r += reader.GetString(0) + "\n";
                }

                roles = r.Split('\n');
                reader.Close();
            }
            else
            {
                user.InnerHtml = "Bağlanamıyor....";
            }
        }

        private void showTable()
        {
            string roles_ = "";
            if(mysql.State == System.Data.ConnectionState.Open)
            {
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM roles", mysql);

                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        roles_ += reader.GetString(1) + "\n";
                    }
                }

                all_of_roles = roles_.Split('\n');
            }

            string html_codes = "<table class='table table-striped' name='table1' id='table1' runat='server'>" + 
                "<thead class='thead-dark'>" +
                "<tr>" +
                "<th scope ='col'>#</th>" +
                "<th scope ='col'>id</th>"; 
                

            for (int i = 0; i < all_of_roles.Length; i++)
            {
                html_codes += "<th scope ='col'>" +all_of_roles[i] + "</th>";
            }

            html_codes += "<th scope ='col' ></ th >" +
                "</tr>" +
                "</thead>";

            

            if(mysql.State == System.Data.ConnectionState.Open)
            {
                MySqlCommand cmd1 = new MySqlCommand("SELECT * FROM datas", mysql);

                using (MySqlDataReader reader = cmd1.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        ids.Add(reader.GetString(0));
                        d0.Add(reader.GetString(1));
                        d1.Add(reader.GetString(2));
                        d2.Add(reader.GetString(3));
                        d3.Add(reader.GetString(4));
                        d4.Add(reader.GetString(5));
                        d5.Add(reader.GetString(6));
                        d6.Add(reader.GetString(7));
                        d7.Add(reader.GetString(8));
                        d8.Add(reader.GetString(9));
                        d9.Add(reader.GetString(10));
                    }
                }

                html_codes += "<tbody runat='server'>";

                for(int i = 0; i < ids.Count; i++)
                {
                    html_codes += "<tr>" +
                        "<th scope='row'>" + (i + 1).ToString() + "</th>" +
                        "<td>" + ids[i] + "</td>" +
                        "<td>" + d0[i] + "</td>" +
                        "<td>" + d1[i] + "</td>" +
                        "<td>" + d2[i] + "</td>" +
                        "<td>" + d3[i] + "</td>" +
                        "<td>" + d4[i] + "</td>" +
                        "<td>" + d5[i] + "</td>" +
                        "<td>" + d6[i] + "</td>" +
                        "<td>" + d7[i] + "</td>" +
                        "<td>" + d8[i] + "</td>" +
                        "<td>" + d9[i] + "</td>" +
                        "<td ><a href='/detail_item.aspx?id=" + ids[i] + "'>Detay</a></td>" + 
                    "</tr>";

                    btn = new Button();
                    btn.ID = "edit";
                    btn.Text = "Düzenle";
                    btn.Click += Btn_Click;
                    user_form.Controls.Add(btn);
                }

                html_codes += "</tbody></table>";
            }

            user_form.InnerHtml = html_codes;
        }

        private void search(string id)
        {

            string html_codes = "<table class='table table-striped' name='table1' id='table1' runat='server'>" +
                "<thead class='thead-dark'>" +
                "<tr>" +
                "<th scope ='col'>#</th>" +
                "<th scope ='col'>id</th>";


            for (int i = 0; i < all_of_roles.Length; i++)
            {
                html_codes += "<th scope ='col'>" + all_of_roles[i] + "</th>";
            }

            html_codes += "<th scope ='col' ></ th >" +
                "</tr>" +
                "</thead>";



            ids.Clear();
            d0.Clear();
            d1.Clear();
            d2.Clear();
            d3.Clear();
            d4.Clear();
            d5.Clear();
            d6.Clear();
            d7.Clear();
            d8.Clear();
            d9.Clear();

            if (mysql.State == System.Data.ConnectionState.Open)
            {
                string cmd_ = "SELECT * FROM datas WHERE data_id='" + id + "'";
                MySqlCommand cmd = new MySqlCommand(cmd_, mysql);

                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        ids.Add(reader.GetString(0));
                        d0.Add(reader.GetString(1));
                        d1.Add(reader.GetString(2));
                        d2.Add(reader.GetString(3));
                        d3.Add(reader.GetString(4));
                        d4.Add(reader.GetString(5));
                        d5.Add(reader.GetString(6));
                        d6.Add(reader.GetString(7));
                        d7.Add(reader.GetString(8));
                        d8.Add(reader.GetString(9));
                        d9.Add(reader.GetString(10));
                    }
                }

                html_codes += "<tbody>";

                for (int i = 0; i < ids.Count; i++)
                {
                    html_codes += "<tr>" +
                        "<th scope='row'>" + (i + 1).ToString() + "</th>" +
                        "<td>" + ids[i] + "</td>" +
                        "<td>" + d0[i] + "</td>" +
                        "<td>" + d1[i] + "</td>" +
                        "<td>" + d2[i] + "</td>" +
                        "<td>" + d3[i] + "</td>" +
                        "<td>" + d4[i] + "</td>" +
                        "<td>" + d5[i] + "</td>" +
                        "<td>" + d6[i] + "</td>" +
                        "<td>" + d7[i] + "</td>" +
                        "<td>" + d8[i] + "</td>" +
                        "<td>" + d9[i] + "</td>" +
                        "<td><button>Sil</button></td>" +
                    "</tr>";
                }

                html_codes += "</tbody></table>";
            }

            user_form.InnerHtml = html_codes;
        }
    }
}