﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dashboard.aspx.cs" Inherits="TeknokentWebSite.dashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Kullanıcı Paneli</title>

    <link rel="stylesheet" type="text/css" href="dashboard_style.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="content" name="content" class="content" runat="server">
            <div id="header" name="header" class="header" runat="server">
                <p id="user" class="user" name="user" runat="server">ssdasdsda</p>
            </div>
        </div>

        <div id="menu" class="menu" name="menu" runat="server">
            <button type="button" class="btn btn-primary" id="opt111" name="opt111" runat="server">Ekle</button>

            <div class="input-group mb-3" id="searchdiv" name="searchdiv">
                <input type="text" class="form-control" placeholder="Ürün Ara" aria-label="Ürün Ara" id="srctext" name="srctext" aria-describedby="button-addon2" runat="server">
                <div class="input-group-append">
                    <input class="btn btn-outline-secondary" type="button" id="button_addon2" name="button_addon2" runat="server" value="Ürün Ara"/>
                </div>
            </div>
            <!--<ul name="list" id="list" class="list" runat="server">
                <!--<li><a href="#" id="opt1" name="opt1" class="opt1" runat="server"><img id="add_img" name="add_img" src="icons8-add-60.png" /></a></li>
                <li><a href="#" id="opt2" name="opt2" class="opt2" runat="server">Ürün Silme</a></li>
                <li><a href="#" id="opt3" name="opt3" class="opt3" runat="server">Ürün Arama</a></li>
                <li><a href="#" id="opt4" name="opt4" class="opt4" runat="server">Ürün Düzenleme</a></li>
            </ul>-->
        </div>

        <div id="user_form" name="user_form" class="user_form" runat="server"> 
            <table class="table table-striped" name="table1" id="table1" runat="server">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">id</th>
                        <th scope="col">data0</th>
                        <th scope="col">data1</th>
                        <th scope="col">data2</th>
                        <th scope="col">data3</th>
                        <th scope="col">data4</th>
                        <th scope="col">data5</th>
                        <th scope="col">data6</th>
                        <th scope="col">data7</th>
                        <th scope="col">data8</th>
                        <th scope="col">data9</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        <td>@mdo</td>
                        <td>@mdo</td>
                        <td><button>Sil</button></td>
                    </tr>
                    
                    <tr>
                        <th scope="row">2</th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                        <td>@fat</td>
                        <td>@fat</td>
                        <td><button>Sil</button></td>
                    </tr>
                    
                    <tr>
                        <th scope="row">3</th>
                        <td>Larry</td>
                        <td>the Bird</td>
                        <td>@twitter</td>
                        <td>Larry</td>
                        <td>the Bird</td>
                        <td>@twitter</td>
                        <td>Larry</td>
                        <td>the Bird</td>
                        <td>@twitter</td>
                        <td>@twitter</td>
                        <td>@twitter</td>
                        <td><button>Sil</button></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
