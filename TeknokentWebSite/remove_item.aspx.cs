﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data;
using MySql.Data.MySqlClient;


namespace TeknokentWebSite
{
    public partial class remove_item : System.Web.UI.Page
    {
        private string user_id = "";
        private bool connected = false;
        private string name = "", sname = "", mail = "", phone = "";
        private string[] roles = new string[] { null };

        private string[] data_ids = new string[] { null };
        private string[] data0 = new string[] { null };
        private string[] data1 = new string[] { null };
        private string[] data2 = new string[] { null };
        private string[] data3 = new string[] { null };
        private string[] data4 = new string[] { null };
        private string[] data5 = new string[] { null };
        private string[] data6 = new string[] { null };
        private string[] data7 = new string[] { null };
        private string[] data8 = new string[] { null };
        private string[] data9 = new string[] { null };

        private MySqlConnection mysql;

        protected void Page_Load(object sender, EventArgs e)
        {
            checkSessions();
            opt1.ServerClick += Opt1_ServerClick;
            opt3.ServerClick += Opt3_ServerClick;
            opt4.ServerClick += Opt4_ServerClick;
        }
        
        private void Opt1_ServerClick(object sender, EventArgs e)
        {
            Session["user_id"] = user_id;
            Response.Redirect("add_item.aspx");
        }
        
        private void Opt3_ServerClick(object sender, EventArgs e)
        {
            Session["user_id"] = user_id;
            Response.Redirect("login.aspx"); /// editlenecek.....
        }

        private void Opt4_ServerClick(object sender, EventArgs e)
        {
            Session["user_id"] = user_id;
            Response.Redirect("login.aspx"); /// editlenecek.....
        }

        private void checkSessions()
        {
            if (Session["user_id"] != null)
            {
                user_id = Session["user_id"].ToString();
                opendb();

                if (connected)
                {
                    getUserInfo(user_id);
                    getAccess(user_id);
                }
            }
            else
            {
                Response.Redirect("login.aspx");
            }
        }

        private void opendb()
        {
            mysql = new MySqlConnection("Database=teknokent_portal;server=127.0.0.1;uid=sqladmin;Password=sqladmin789_");
            mysql.Open();

            if (mysql.State == System.Data.ConnectionState.Open)
            {
                connected = true;
            }
            else
            {
                connected = false;
            }
        }

        private void getUserInfo(string id)
        {
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM users WHERE user_id='" + id + "'", mysql);
            MySqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                name = reader["name"].ToString();
                sname = reader["sname"].ToString();
                mail = reader["email"].ToString();
                phone = reader["phone"].ToString();

                user.InnerHtml = "Hoşgeldiniz " + name + " " + sname;
            }
            else
            {
                content.InnerHtml = "<script>alert('Kişi bilgileri yuklenirken hata olustu...');</script>";
                Response.Redirect("login.aspx");
            }

            reader.Close();
        }

        private void getAccess(string id)
        {
            if (mysql.State == System.Data.ConnectionState.Open)
            {
                MySqlCommand cmd = new MySqlCommand("SELECT role_id FROM user_roles WHERE user_id='" + id + "'", mysql);
                MySqlDataReader reader = cmd.ExecuteReader();

                string r = "";
                while (reader.Read())
                {
                    r += reader.GetString(0) + "\n";
                }

                roles = r.Split('\n');
                reader.Close();
            }
            else
            {
                user.InnerHtml = "Bağlanamıyor....";
            }
        }

        private void getList()
        {

            string d0 = "", d1 = "", d2 = "", d3 = "", d4 = "", d5 = "", d6 = "", d7 = "", d8 = "", d9 = "";

            string ids = "";

            MySqlCommand cmd = new MySqlCommand("SELECT * FROM datas", mysql);

            using (MySqlDataReader reader = cmd.ExecuteReader()) 
            {
                while (reader.Read())
                {
                    ids += reader.GetString(0) + "\n";
                    d0 += reader.GetString(1) + "\n";
                    d1 += reader.GetString(2) + "\n";
                    d2 += reader.GetString(3) + "\n";
                    d3 += reader.GetString(4) + "\n";
                    d4 += reader.GetString(5) + "\n";
                    d5 += reader.GetString(6) + "\n";
                    d6 += reader.GetString(7) + "\n";
                    d7 += reader.GetString(8) + "\n";
                    d8 += reader.GetString(9) + "\n";
                    d9 += reader.GetString(10) + "\n";
                }

                data_ids = ids.Split('\n');
                data0 = d0.Split('\n');
                data1 = d1.Split('\n');
                data2 = d2.Split('\n');
                data3 = d3.Split('\n');
                data4 = d4.Split('\n');
                data5 = d5.Split('\n');
                data6 = d6.Split('\n');
                data7 = d7.Split('\n');
                data8 = d8.Split('\n');
                data9 = d9.Split('\n');


                
            }           
        }
    }
}