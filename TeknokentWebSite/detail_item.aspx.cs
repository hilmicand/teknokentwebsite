﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace TeknokentWebSite
{
    public partial class detail_item : System.Web.UI.Page
    {
        private string user_id = "";
        private bool connected = false;
        private MySqlConnection mysql;
        private string name = "", sname = "", mail = "", phone = "";

        private string[] roles = new string[] { null };
        private string[] role_names;
        private ArrayList role_datas;
        private string item_id = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            product_id.InnerHtml = "ID : " + Request.QueryString["id"];

            if (Session["user_id"] != null)
            {
                user_id = Session["user_id"].ToString();
                opendb();

                if (connected)
                {
                    getUserInfo(user_id);
                    getAccess(user_id);
                    item_id = Request.QueryString["id"];
                    loadItems(item_id);
                    setInterface();

                    edit_.ServerClick += Edit__ServerClick;
                    delete.ServerClick += Delete_ServerClick;
                }
            }
            else
            {
                Response.Redirect("login.aspx");
            }
        }

        private void Delete_ServerClick(object sender, EventArgs e)
        {
            deleteProduct(item_id);
        }
        
        private void Edit__ServerClick(object sender, EventArgs e)
        {
            saveChanges();
        }

        private void checkSessions()
        {
            if (Session["user_id"] != null)
            {
                user_id = Session["user_id"].ToString();
                opendb();

                if (connected)
                {
                    getUserInfo(user_id);
                    getAccess(user_id);
                    setInterface();
                }
            }
            else
            {
                Response.Redirect("login.aspx");
            }
        }

        private void opendb()
        {
            mysql = new MySqlConnection("Database=teknokent_portal;server=127.0.0.1;uid=sqladmin;Password=sqladmin789_");
            mysql.Open();

            if (mysql.State == System.Data.ConnectionState.Open)
            {
                connected = true;
            }
            else
            {
                connected = false;
            }
        }

        private void getUserInfo(string id)
        {
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM users WHERE user_id='" + id + "'", mysql);
            MySqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                name = reader["name"].ToString();
                sname = reader["sname"].ToString();
                mail = reader["email"].ToString();
                phone = reader["phone"].ToString();

                user.InnerHtml = "Hoşgeldiniz " + name + " " + sname;
            }
            else
            {
                content.InnerHtml = "<script>alert('Kişi bilgileri yuklenirken hata olustu...');</script>";
                Response.Redirect("login.aspx");
            }

            reader.Close();
        }

        private void getAccess(string id)
        {
            if (mysql.State == System.Data.ConnectionState.Open)
            {
                MySqlCommand cmd = new MySqlCommand("SELECT role_id FROM user_roles WHERE user_id='" + id + "'", mysql);
                MySqlDataReader reader = cmd.ExecuteReader();

                string r = "";
                while (reader.Read())
                {
                    r += reader.GetString(0) + "\n";
                }

                roles = r.Split('\n');
                role_names = new string[roles.Length];
                reader.Close();
            }
            else
            {
                user.InnerHtml = "Bağlanamıyor....";
            }
        }

        private string getAccessName(string role_id)
        {
            string a = "";

            if (mysql.State == System.Data.ConnectionState.Open)
            {
                MySqlCommand cmd = new MySqlCommand("SELECT role_name FROM roles WHERE role_id='" + role_id + "'", mysql);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        a = reader["role_name"].ToString();
                    }
                }
            }

            return a;
        }

        private void setInterface()
        {
            string form_data = "";
            string[] divnames = new string[roles.Length];
            /// Arayuz olusturma.....
            for (int i = 0; i < roles.Length - 1; i++)
            {
                role_names[i] = getAccessName(roles[i]);
                divnames[i] = "div" + i.ToString();
                form_data += "<div id='item' name='item' class='item' style='width: 210px; float: left; margin-left: 30px; margin-top: 20px;'>"
                                    + "<p>" + role_names[i] + "</p>"
                                    + "<input id='val" + i.ToString() + "' name='val" + i.ToString() + "' type='text' placeholder='" + role_names[i] + " degerini giriniz' " +
                                        "style='width: 190px; height: 35px; border-radius: 4px; padding-left: 7px;' value='" + role_datas[i] +"'/>"
                                    + "</div>";
            }

            if (exp3.InnerHtml.Contains(role_names[0]))

            {

            }
            else
            {
                exp3.InnerHtml = form_data;
            }

        }

        private void getValues()
        {
            string[] datas = new string[role_names.Length - 1];

            for (int i = 0; i < datas.Length; i++)
            {
                datas[i] = Request["val" + i.ToString()]; /// id si val0 olan HTML input elementinden (textbox aslinda) data alma.....
            }

            string cmd_name = "";

            for (int i = 0; i < roles.Length - 1; i++)
            {
                if (i != roles.Length - 2)
                {
                    cmd_name += "data" + (int.Parse(roles[i]) - 1).ToString() + ", ";
                }
                else
                {
                    cmd_name += "data" + (int.Parse(roles[i]) - 1).ToString();
                }
            }


            var data_id = (int)(DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalSeconds;


            string cnd = "INSERT INTO datas (data_id, " + cmd_name + ") VALUES('" + data_id + "', ";

            for (int i = 0; i < datas.Length; i++)
            {
                if (i != datas.Length - 1)
                {
                    cnd += "'" + datas[i] + "', ";
                }
                else
                {
                    cnd += "'" + datas[i] + "')";
                }
            }

            MySqlCommand command = new MySqlCommand(cnd, mysql);
            int result = command.ExecuteNonQuery();

            user.InnerHtml = "Result : " + result.ToString();

            if (result == 1)
            {
                user.InnerHtml = "Ekleme işlemi başarılı....";
            }
            else
            {
                user.InnerHtml = "Eklenemedi....";
            }
        }

        private void loadItems(string id)
        {
            if(mysql.State == System.Data.ConnectionState.Open)
            {
                role_datas = new ArrayList();
                string cmd_ = "SELECT * FROM datas WHERE data_id='" + id + "'";
                MySqlCommand cmd = new MySqlCommand(cmd_, mysql);

                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        role_datas.Add(reader.GetString(1));
                        role_datas.Add(reader.GetString(2));
                        role_datas.Add(reader.GetString(3));
                        role_datas.Add(reader.GetString(4));
                        role_datas.Add(reader.GetString(5));
                        role_datas.Add(reader.GetString(6));
                        role_datas.Add(reader.GetString(7));
                        role_datas.Add(reader.GetString(8));
                        role_datas.Add(reader.GetString(9));
                        role_datas.Add(reader.GetString(10));
                    }
                }


            }
        }

        private void saveChanges()
        {
            string[] datas = new string[role_names.Length - 1];

            for (int i = 0; i < datas.Length; i++)
            {
                datas[i] = Request["val" + i.ToString()]; /// id si val0 olan HTML input elementinden (textbox aslinda) data alma.....
            }

            string cmd_name = "";

            for (int i = 0; i < roles.Length - 1; i++)
            {
                if (i != roles.Length - 2)
                {
                    cmd_name += "data" + (int.Parse(roles[i]) - 1).ToString() + ", ";
                }
                else
                {
                    cmd_name += "data" + (int.Parse(roles[i]) - 1).ToString();
                }
            }


            var data_id = (int)(DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalSeconds;


            string cnd = "UPDATE datas SET ";

            for (int i = 0; i < datas.Length; i++)
            {
                if(i != datas.Length - 1)
                {
                    cnd += " data" + i.ToString() + "='" +datas[i] + "',";
                }
                else
                {
                    cnd += " data" + i.ToString() + "='" + datas[i] + "'";
                }
            }

            cnd += " WHERE data_id='" + item_id + "'";

            user.InnerHtml = cnd;


            MySqlCommand command = new MySqlCommand(cnd, mysql);
            int result = command.ExecuteNonQuery();

            user.InnerHtml = "Result : " + result.ToString();

            if (result == 1)
            {
                user.InnerHtml = "Değişiklikler kaydedildi.";
                Session["user_id"] = user_id;
                Response.Redirect("dashboard.aspx");
            }
            else
            {
                user.InnerHtml = "Bir hata oluştu.";
            }
        }

        private void deleteProduct(string item_id)
        {
            if(mysql.State == System.Data.ConnectionState.Open)
            {
                string cmd_ = "DELETE FROM datas WHERE data_id='" + item_id + "'";
                MySqlCommand cmd1 = new MySqlCommand(cmd_, mysql);

                if(cmd1.ExecuteNonQuery() == 1)
                {
                    Session["user_id"] = user_id;
                    Response.Redirect("dashboard.aspx");
                }
            }
        }
    }
}