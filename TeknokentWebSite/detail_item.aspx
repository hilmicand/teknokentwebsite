﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="detail_item.aspx.cs" Inherits="TeknokentWebSite.detail_item" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Ürün Detayı</title>
    <link rel="stylesheet" type="text/css" href="detail_item_style.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

</head>
<body>
     <form id="form1" runat="server">
        <div id="content" name="content" class="content" runat="server">
            <div id="header" name="header" class="header" runat="server">
                <p id="user" class="user" name="user" runat="server">Ürün Detayı</p>
            </div>
        </div>

        <div id="menu" class="menu" name="menu" runat="server">
            <button type="button" class="btn btn-primary" id="edit_" name="edit_" runat="server">Değişiklikleri Kaydet</button>
            <button type="button" class="btn btn-danger" id="delete" name="delete" runat="server">Sil</button>
        </div>

        <div id="user_form" name="user_form" class="user_form" runat="server"> 
            <p id="product_id" name="product_id" runat="server"></p>

            <div id="exp3" name="exp3" class="exp3" runat="server">

            </div>
        </div>
    </form>
</body>
</html>
