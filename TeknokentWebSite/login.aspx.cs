﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using MySql.Data;

namespace TeknokentWebSite
{
    public partial class login : System.Web.UI.Page
    {
        private MySqlConnection mySql;

        protected void Page_Load(object sender, EventArgs e)
        {
            login_btn.ServerClick += Login_btn_ServerClick;
            Session.Add("user_id", "");

            checkConnection();
        }

        private void Login_btn_ServerClick(object sender, EventArgs e)
        {
            MySqlConnectionSample();
        }

        private void checkConnection()
        {
            mySql = new MySqlConnection("Database=teknokent_portal;server=127.0.0.1;uid=sqladmin;Password=sqladmin789_");
            mySql.Open();

            switch (mySql.State)
            {
                case System.Data.ConnectionState.Open:
                    status.InnerHtml = "Bağlantı var";
                    break;

                case System.Data.ConnectionState.Broken:
                    status.InnerHtml = "Bağlantı yok...";
                    break;
            }
        }

        private void MySqlConnectionSample()
        {
            switch (mySql.State)
            {
                case System.Data.ConnectionState.Open:
                    status.InnerHtml = "Bağlantı var";
                    MySqlCommand cmd = new MySqlCommand("SELECT * FROM users WHERE user_='" + user.Value + "' AND pass='" + pass.Value + "'", mySql);
                    MySqlDataReader reader = cmd.ExecuteReader();

                    if(!reader.Read()) {
                        status.InnerHtml = "Kişi bulunamadı...";
                    } else {
                        string name = "", sname = "", id = "";
                        name = reader["name"].ToString();
                        sname = reader["sname"].ToString();
                        id = reader["user_id"].ToString();
                        

                        Console.WriteLine("name : " + name);
                        Console.WriteLine("sname : " + sname);
                        status.InnerHtml = "Selam " + name + " " + sname + " id : " + id;
                        Session["user_id"] = id;
                        Response.Redirect("dashboard.aspx", false);
                        reader.Close();
                        mySql.Close();
                    }
                    break;

                case System.Data.ConnectionState.Broken:
                    status.InnerHtml = "Bağlantı yok...";
                    break;
            }
        }
    }
}