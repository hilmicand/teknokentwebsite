﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add_item.aspx.cs" Inherits="TeknokentWebSite.add_item" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Ürün Ekleme</title>

    <link rel="stylesheet" type="text/css" href="add_item_style.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

</head>
<body>
    <form id="form1" runat="server">
        <div id="content" name="content" class="content" runat="server">
            <div id="header" name="header" class="header" runat="server">
                <p id="user" class="user" name="user" runat="server">MCBÜ Teknokent Web Portalı Denemesi</p>
            </div>
        </div>

        <div id="menu" class="menu" name="menu" runat="server">
            <ul name="list" id="list" class="list" runat="server">
                <li><a id="opt1" name="opt1" class="opt1" runat="server">Ürün Ekleme</a></li>
                <li><a href="#" id="opt2" name="opt2" class="opt2" runat="server">Ürün Silme</a></li>
                <li><a href="#" id="opt3" name="opt3" class="opt3" runat="server">Ürün Arama</a></li>
                <li><a href="#" id="opt4" name="opt4" class="opt4" runat="server">Ürün Düzenleme</a></li>
            </ul>
        </div>

        <div id="user_form" name="user_form" class="user_form" runat="server">

            <div id="exp2" name="exp2" class="exp2">
                <input class="btn btn-primary" type="button" name="save_btn" id="save_btn" value="Kaydet" runat="server">
                <input class="btn btn-secondary" type="button" name="cancel_btn" id="cancel_btn" value="İptal" runat="server">
            </div>
            </br>

            <div id="exp3" name="exp3" class="exp3" runat="server">

            </div>
        </div>
    </form>
</body>
</html>
