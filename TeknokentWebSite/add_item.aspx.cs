﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace TeknokentWebSite
{
    public partial class add_item : System.Web.UI.Page
    {
        private string user_id = "";
        private string name = "", sname = "", mail = "", phone = "";
        private string[] roles = new string[] { null };
        private string[] role_names;
        private bool connected = false;

        private MySqlConnection mysql;

        protected void Page_Load(object sender, EventArgs e)
        {
            checkSessions();
            opt2.ServerClick += Opt2_ServerClick;
            opt3.ServerClick += Opt3_ServerClick;
            opt4.ServerClick += Opt4_ServerClick;
            save_btn.ServerClick += Save_btn_ServerClick;
        }

        private void Save_btn_ServerClick(object sender, EventArgs e)
        {
            getValues();
        }

        private void Opt2_ServerClick(object sender, EventArgs e)
        {
            Session["user_id"] = user_id;
            Response.Redirect("remove_item.aspx");
        }
        
        private void Opt3_ServerClick(object sender, EventArgs e)
        {
            Session["user_id"] = user_id;
            Response.Redirect("login.aspx"); /// Editlenecek
        }

        private void Opt4_ServerClick(object sender, EventArgs e)
        {
            Session["user_id"] = user_id;
            Response.Redirect("login.aspx"); /// Editlenecek....
        }

        #region standart yapilandirmalar (Session kontrol, db acma, user bilgisi, user_role kontrol)
        private void checkSessions()
        {
            if (Session["user_id"] != null)
            {
                user_id = Session["user_id"].ToString();
                opendb();

                if (connected)
                {
                    getUserInfo(user_id);
                    getAccess(user_id);
                    setInterface();
                }
            }
            else
            {
                Response.Redirect("login.aspx");
            }
        }

        private void opendb()
        {
            mysql = new MySqlConnection("Database=teknokent_portal;server=127.0.0.1;uid=sqladmin;Password=sqladmin789_");
            mysql.Open();

            if (mysql.State == System.Data.ConnectionState.Open)
            {
                connected = true;
            }
            else
            {
                connected = false;
            }
        }

        private void getUserInfo(string id)
        {
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM users WHERE user_id='" + id + "'", mysql);
            MySqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                name = reader["name"].ToString();
                sname = reader["sname"].ToString();
                mail = reader["email"].ToString();
                phone = reader["phone"].ToString();

                user.InnerHtml = "Hoşgeldiniz " + name + " " + sname;
            }
            else
            {
                content.InnerHtml = "<script>alert('Kişi bilgileri yuklenirken hata olustu...');</script>";
                Response.Redirect("login.aspx");
            }

            reader.Close();
        }

        private void getAccess(string id)
        {
            if (mysql.State == System.Data.ConnectionState.Open)
            {
                MySqlCommand cmd = new MySqlCommand("SELECT role_id FROM user_roles WHERE user_id='" + id + "'", mysql);
                MySqlDataReader reader = cmd.ExecuteReader();

                string r = "";
                while (reader.Read())
                {
                    r += reader.GetString(0) + "\n";
                }

                roles = r.Split('\n');
                role_names = new string[roles.Length];
                reader.Close();
            }
            else
            {
                user.InnerHtml = "Bağlanamıyor....";
            }
        }
        #endregion

        private string getAccessName(string role_id)
        {
            string a = "";

            if (mysql.State == System.Data.ConnectionState.Open)
            {
                MySqlCommand cmd = new MySqlCommand("SELECT role_name FROM roles WHERE role_id='" + role_id + "'", mysql);
                using (MySqlDataReader reader = cmd.ExecuteReader()) {
                    if (reader.Read())
                    {
                        a = reader["role_name"].ToString();
                    }
                }
            }

            return a;
        }
         

        private void setInterface()
        {
            string form_data = "";
            string[] divnames = new string[roles.Length];
            /// Arayuz olusturma.....
            for (int i = 0; i < roles.Length - 1; i++)
            {
                role_names[i] = getAccessName(roles[i]);
                divnames[i] = "div" + i.ToString();
                form_data += "<div id='item' name='item' class='item' style='width: 210px; float: left; margin-left: 30px; margin-top: 20px;'>"
                                    + "<p>" + role_names[i] + "</p>"
                                    + "<input id='val" + i.ToString() + "' name='val" + i.ToString() + "' type='text' placeholder='" + role_names[i] + " degerini giriniz' " +
                                        "style='width: 190px; height: 35px; border-radius: 4px; padding-left: 7px;'/>"
                                    +"</div>";
            }

            if(exp3.InnerHtml.Contains(role_names[0]))

            {

            } else
            {
                exp3.InnerHtml = form_data;
            }
            
        }

        private void getValues()
        {
            string[] datas = new string[role_names.Length - 1];

            for (int i = 0; i < datas.Length; i++)
            {
                datas[i] = Request["val" + i.ToString()]; /// id si val0 olan HTML input elementinden (textbox aslinda) data alma.....
            }

            string cmd_name = "";

            for (int i = 0; i < roles.Length - 1; i++)
            {
                if(i != roles.Length - 2)
                {
                    cmd_name += "data" + (int.Parse(roles[i]) - 1).ToString() + ", ";
                }
                else
                {
                    cmd_name += "data" + (int.Parse(roles[i]) - 1).ToString();
                }
            }


            var data_id = (int)(DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalSeconds;


            string cnd = "INSERT INTO datas (data_id, " + cmd_name + ") VALUES('" + data_id + "', ";

            for (int i = 0; i < datas.Length; i++)
            {
                if(i != datas.Length - 1)
                {
                    cnd += "'" + datas[i] + "', ";
                }
                else
                {
                    cnd += "'" + datas[i] + "')";
                }     
            }
            
            MySqlCommand command = new MySqlCommand(cnd, mysql);
            int result = command.ExecuteNonQuery();

            user.InnerHtml = "Result : " + result.ToString();

            if(result == 1)
            {
                user.InnerHtml = "Ekleme işlemi başarılı....";
            }
            else
            {
                user.InnerHtml = "Eklenemedi....";
            }
        }
    }
}